Source: cds-healpix-java
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: java
Priority: optional
Build-Depends: ant,
               ant-optional,
               debhelper-compat (= 13),
               default-jdk,
               javahelper,
               junit4,
               libcommons-math3-java
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/cds-healpix-java
Vcs-Git: https://salsa.debian.org/debian-astro-team/cds-healpix-java.git
Homepage: https://github.com/cds-astro/cds-healpix-java

Package: libcds-healpix-java
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends}
Recommends: ${java:Recommends}
Description: CDS HEALPix library in Java
 HEALPix is an acronym for Hierarchical Equal Area isoLatitude
 Pixelization of a sphere. As suggested in the name, this pixelization
 produces a subdivision of a spherical surface in which each pixel
 covers the same surface area as every other pixel. It is commonly
 used to store all-sky astronomical images, most famously maps of the
 cosmic microwave background.

Package: libcds-healpix-java-doc
Architecture: all
Section: doc
Depends: ${java:Depends},
         ${misc:Depends}
Recommends: ${java:Recommends}
Description: API documentation for the CDS HEALPix library in Java
 HEALPix is an acronym for Hierarchical Equal Area isoLatitude
 Pixelization of a sphere. As suggested in the name, this pixelization
 produces a subdivision of a spherical surface in which each pixel
 covers the same surface area as every other pixel. It is commonly
 used to store all-sky astronomical images, most famously maps of the
 cosmic microwave background.
 .
 This package contains the Javadoc API.
